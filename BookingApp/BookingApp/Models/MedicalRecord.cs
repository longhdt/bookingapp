﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingApp.Models
{
    public class Customer
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public DateTime DayOfBirth { get; set; }
    }

    public class Doctor
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class Room
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class Service
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class MedicalRecord
    {
        public Customer Customer { get; set; }
        public Doctor Doctor { get; set; }
        public Room Room { get; set; }
        public Service Service { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingApp.Businesses;
using BookingApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BookingApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IBookingBusiness _bookingBusiness;
        private readonly ILogger<BookingController> _logger;

        public BookingController(IBookingBusiness bookingBusiness, ILogger<BookingController> logger)
        {
            _logger = logger;
            _bookingBusiness = bookingBusiness;
        }

        [HttpPost]
        public IActionResult Post(MedicalRecord medicaRecord)
        {
            _logger.LogInformation(JsonConvert.SerializeObject(medicaRecord));
            _bookingBusiness.Create(medicaRecord);
            return Ok();
        }
    }
}
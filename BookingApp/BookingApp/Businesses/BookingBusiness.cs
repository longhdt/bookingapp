﻿using BookingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookingApp.Businesses
{
    public interface IBookingBusiness
    {
        bool Create(MedicalRecord medicalRecord);
    }

    public class BookingBusiness : IBookingBusiness
    {
        public bool Create(MedicalRecord medicalRecord)
        {

            return true;
        }
    }
}
